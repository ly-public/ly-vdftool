package com.lyoneel.lyvdftool

import com.google.gson.GsonBuilder
import java.io.*
import java.nio.charset.Charset
import java.util.*

class LyVDFTool {
    companion object {
        private var REGEX_KEY_BLOCK = Regex(""""(.*?)"$""")
        private val parser: LyVDFParser = LyVDFParser()

        /**
         * Parses a VDF target file using charset specified by default charset is UTF-8
         */
        fun parseFromFile(targetFile: String, charset: Charset = Charsets.UTF_8): SortedMap<String, Any> {
            val file = File(targetFile)
            val output: SortedMap<String, Any>

            output = if( file.exists() ) {
                parser.parse(file.readLines(charset))
            } else {
                throw IOException("File not found $targetFile")
            }

            return output
        }

        /**
         * Parses a VDF multi-line string
         */
        fun parseFromString(input: String): SortedMap<String, Any> {
            return parser.parse(input.split("\n").toList())
        }

        /**
         * Parses a VDF list of strings
         */
        fun parseFromList(input: List<String>): SortedMap<String, Any> {
            return parser.parse(input)
        }

        /**
         * Search inside file the target block using keyword
         */
        fun searchAndParseFromFile(keyWord: String, targetFile: String): SortedMap<String, Any> {
            val file = File(targetFile)
            val output: SortedMap<String, Any>

            if( file.exists() ) {
                val reader = BufferedReader(FileReader(file))
                var targetBlock: MutableList<String> = arrayListOf()
                var foundTarget = false
                var openBraces = 0

                reader.lines().use { lines ->
                    for(it in lines){
                        if(foundTarget){
                            targetBlock.add(it)
                            if(it.trim().contains("{")){
                                openBraces++
                            }

                            if(it.trim().contains("}")){
                                openBraces--
                            }

                            if(openBraces == 0){
                                break
                            }
                        }

                        if(it.contains(keyWord) && REGEX_KEY_BLOCK.matches(it.trim())){
                            foundTarget = true
                            targetBlock.add(it)
                        }
                    }
                }
                output = parser.parse(targetBlock)
            } else {
                throw IOException("File not found $targetFile")
            }

            return output
        }

        /**
         * Converts map to JSON using Gson
         */
        fun mapToJSON(objectToMap: SortedMap<String, Any>): String {
            return GsonBuilder().create().toJson(objectToMap, Map::class.java)
        }
    }
}