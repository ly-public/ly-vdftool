package com.lyoneel.lyvdftool

import java.util.*

/**
 * This is a class that parses Valve's VDF files and returns a Map.
 * Currently supports:
 * - Key-Values
 * - Key-Block values
 * - Key-Multi line values
 */
class LyVDFParser {

    companion object {
        private const val BRACKET_OPEN = "{"
        private const val BRACKET_CLOSE = "}"
        private var REGEX_KEY_BLOCK = Regex(""""(.*?)"$""")
        private var REGEX_KEY_VALUE = Regex(""""(.*?)"\s+"(.*?)"$""")
        private var REGEX_KEY_VALUE_MULTI_START = Regex(""""(.*?)"\s+"(.*?)$""")
        private var REGEX_KEY_VALUE_MULTI_END = Regex("""(.*?)"$""")
        private var globalIndex: Int = -1
    }

    /**
     * This class will handle one or more processing blocks from root if needed
     */
    fun parse(content: List<String>): SortedMap<String, Any>{
        val output: SortedMap<String, Any> = sortedMapOf()

        println("Parsing...")

        while( globalIndex < content.size){
            processBlock(content, output)
        }

        println("Parsed ${content.size} lines.")

        globalIndex = -1

        return output
    }

    /**
     * Process a block of values, uses recursive calls to process blocks inside blocks
     */
    private fun processBlock(input: List<String>, output: SortedMap<String, Any>): SortedMap<String, Any> {
        var foundClosingBracket = false;
        globalIndex++;

        while(!foundClosingBracket && globalIndex < input.size){
            val workingLine = input[globalIndex].trim();

            when {
                workingLine == BRACKET_CLOSE -> {
                    foundClosingBracket = true;
                    globalIndex++
                }

                isKeyLine(workingLine) && input[globalIndex +1].trim() == BRACKET_OPEN -> {
                    val key = processKeyLine(input[globalIndex])
                    globalIndex++
                    val value = processBlock(input, sortedMapOf())
                    output[key] = value
                }

                isKeyValueLine(workingLine) -> {
                    processKeyValueLine(workingLine, output)
                    globalIndex++
                }

                isKeyValueMultiLine(workingLine) -> {
                    val offset = processKeyValueMultiLine(input, workingLine, output)
                    globalIndex += offset + 1
                }

                else -> {
                    // Not recognized lines goes here
                    println("Line ignored ${workingLine}, in index number $globalIndex")
                    globalIndex++
                }
            }
        }

        return output;
    }

    /**
     * Checks if this line is a valid Key-Value
     */
    private fun isKeyValueLine(line: String): Boolean {
        return REGEX_KEY_VALUE.matches(line)
    }

    /**
     * Obtains values (key-values) from working line
     */
    private fun processKeyValueLine(line: String, output: SortedMap<String, Any>): Boolean {
        val matcher = REGEX_KEY_VALUE.find(line)
        var result = false

        if (matcher!!.groups.size == 3) {
            val key =   matcher.groups[1]!!.value
            val value = matcher.groups[2]!!.value
            output[key] = value;
            result = true
        }

        return result
    }

    /**
     * Checks if this line is a valid Key
     */
    private fun isKeyLine(line: String): Boolean {
        return REGEX_KEY_BLOCK.matches(line)
    }

    /**
     * Obtains key from working line
     */
    private fun processKeyLine(line: String): String {
        val matcher = REGEX_KEY_BLOCK.find(line)
        return matcher!!.groups[1]!!.value
    }

    /**
     * Checks if this line is a valid multiline-value
     */
    private fun isKeyValueMultiLine(line: String): Boolean {
        return REGEX_KEY_VALUE_MULTI_START.matches(line)
    }

    /**
     * Obtains values from a multi-line input and returns the offset counting number of processed lines
     */
    private fun processKeyValueMultiLine(input: List<String>, line: String, output: SortedMap<String, Any>): Int {
        val matcher = REGEX_KEY_VALUE_MULTI_START.find(line)
        var localIndex = globalIndex;

        // First line process
        if (matcher!!.groups.size == 3) {
            val key =   matcher.groups[1]!!.value
            var value = matcher.groups[2]!!.value
            localIndex++
            // Find next ending quote
            while( !REGEX_KEY_VALUE_MULTI_END.matches(input[localIndex]) && localIndex < input.size) {
                value += "\n${input[localIndex]}"
                localIndex++
            }
            val lastLineMatcher = REGEX_KEY_VALUE_MULTI_END.find(input[localIndex])
            value += "\n${lastLineMatcher!!.groups[1]!!.value}";

            output[key] = value;
        }

        return localIndex - globalIndex
    }

}
