package com.lyoneel.lyvdftool

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.Test
import java.util.*

@Suppress("UNCHECKED_CAST")
internal class LyVDFParserTest {

    val FILES_ROOT = "./src/test/resources/files"

    @Test
    fun parseMLString() {

        val input = """ "thisIsMyKeyBlock"
            {
            "key" "value"
            "key1"    "value1"
            }
            """
        val vdfMap = LyVDFTool.parseFromString(input);

        assertThat(vdfMap, notNullValue())
        assertThat(vdfMap.size, equalTo(1))
        assertThat(vdfMap.containsKey("thisIsMyKeyBlock"), equalTo(true))

        val keyBlock = vdfMap["thisIsMyKeyBlock"] as SortedMap<String, String>

        assertThat(keyBlock.size, equalTo(2))
        assertThat(keyBlock.containsKey("key"), equalTo(true))
        assertThat(keyBlock.containsKey("key1"), equalTo(true))
        assertThat(keyBlock["key"], equalTo("value"))
        assertThat(keyBlock["key1"], equalTo("value1"))


    }


    @Test
    fun parseMLStringMultiLineValue() {

        val input = """ "thisIsMyKeyBlock"
            {
            "key" "value"
            "key1"    "line1value
line2value
line3value"
            }
            """
        val vdfMap = LyVDFTool.parseFromString(input);

        assertThat(vdfMap, notNullValue())
        assertThat(vdfMap.size, equalTo(1))

        val keyBlock = vdfMap["thisIsMyKeyBlock"] as SortedMap<String, String>

        assertThat(vdfMap.containsKey("thisIsMyKeyBlock"), equalTo(true))
        assertThat(keyBlock.size, equalTo(2))
        assertThat(keyBlock.containsKey("key"), equalTo(true))
        assertThat(keyBlock.containsKey("key1"), equalTo(true))
        assertThat(keyBlock["key"], equalTo("value"))
        assertThat(keyBlock["key1"], equalTo("line1value\nline2value\nline3value"))


    }


    @Test
    fun parseFileSimpleBlock() {

        val targetFile = "$FILES_ROOT/simple-block.vdf"
        val vdfMap = LyVDFTool.parseFromFile(targetFile);

        assertThat(vdfMap, notNullValue())
        assertThat(vdfMap.size, equalTo(1))
        assertThat(vdfMap.containsKey("thisIsMyKeyBlock"), equalTo(true))

        val keyBlock = vdfMap["thisIsMyKeyBlock"] as SortedMap<String, String>

        assertThat(keyBlock.size, equalTo(2))
        assertThat(keyBlock.containsKey("key"), equalTo(true))
        assertThat(keyBlock.containsKey("key1"), equalTo(true))
        assertThat(keyBlock["key"], equalTo("value"))
        assertThat(keyBlock["key1"], equalTo("value1"))


    }


    @Test
    fun parseFileSimpleBlockMultilinesValues() {

        val targetFile = "$FILES_ROOT/simple-block_multiline-values.vdf"
        val vdfMap = LyVDFTool.parseFromFile(targetFile);

        assertThat(vdfMap, notNullValue())
        assertThat(vdfMap.size, equalTo(1))

        val keyBlock = vdfMap["thisIsMyKeyBlock"] as SortedMap<String, String>

        assertThat(vdfMap.containsKey("thisIsMyKeyBlock"), equalTo(true))
        assertThat(keyBlock.size, equalTo(2))
        assertThat(keyBlock.containsKey("key"), equalTo(true))
        assertThat(keyBlock.containsKey("key1"), equalTo(true))
        assertThat(keyBlock["key"], equalTo("value"))
        assertThat(keyBlock["key1"], equalTo("line1value\nline2value\nline3value"))

    }

    @Test
    fun parseFileComplexBlock() {

        val targetFile = "$FILES_ROOT/complex-block.vdf"
        val vdfMap = LyVDFTool.parseFromFile(targetFile);

        assertThat(vdfMap, notNullValue())
        assertThat(vdfMap.size, equalTo(1))

        val rootBlock = vdfMap["rootBlock"] as SortedMap<String, Any>

        assertThat(vdfMap.containsKey("rootBlock"), equalTo(true))
        assertThat(rootBlock.size, equalTo(4))
        assertThat(rootBlock.containsKey("key1"), equalTo(true))
        assertThat(rootBlock.containsKey("key2"), equalTo(true))
        assertThat(rootBlock.containsKey("child1"), equalTo(true))
        assertThat(rootBlock.containsKey("child2"), equalTo(true))

        assertThat(rootBlock["key1"] as String, equalTo("value1"))
        assertThat(rootBlock["key2"] as String, equalTo("line1value2\nline2value2\nline3value2"))

        val child1 = rootBlock["child1"] as SortedMap<String, Any>

        assertThat(child1["child1key1"] as String, equalTo("child1value1"))
        assertThat(child1["child1key2"] as String, equalTo("child1value2"))

        val child2 = rootBlock["child2"] as SortedMap<String, Any>

        assertThat(child2["child2key1"] as String, equalTo("child2value1"))
        assertThat(child2["child2key2"] as String, equalTo("child2value2"))

    }

    @Test
    fun parseFileComplexBlockMalformatted() {

        val targetFile = "$FILES_ROOT/complex-block_malformatted.vdf"
        val vdfMap = LyVDFTool.parseFromFile(targetFile);

        assertThat(vdfMap, notNullValue())
        assertThat(vdfMap.size, equalTo(1))

        val rootBlock = vdfMap["rootBlock"] as SortedMap<String, Any>

        assertThat(vdfMap.containsKey("rootBlock"), equalTo(true))
        assertThat(rootBlock.size, equalTo(4))
        assertThat(rootBlock.containsKey("key1"), equalTo(true))
        assertThat(rootBlock.containsKey("key2"), equalTo(true))
        assertThat(rootBlock.containsKey("child1"), equalTo(true))
        assertThat(rootBlock.containsKey("child2"), equalTo(true))

        assertThat(rootBlock["key1"] as String, equalTo("value1"))
        assertThat(rootBlock["key2"] as String, equalTo("line1value2\nline2value2\nline3value2"))

        val child1 = rootBlock["child1"] as SortedMap<String, Any>

        assertThat(child1["child1key1"] as String, equalTo("child1value1"))
        assertThat(child1["child1key2"] as String, equalTo("child1value2"))

        val child2 = rootBlock["child2"] as SortedMap<String, Any>

        assertThat(child2["child2key1"] as String, equalTo("child2value1"))
        assertThat(child2["child2key2"] as String, equalTo("child2value2"))

    }

    @Test
    fun noGarbageTest(){
        parseFileComplexBlock()
        parseFileComplexBlockMalformatted()
    }


}
