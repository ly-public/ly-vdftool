package com.lyoneel.lyvdftool

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import java.util.*

@Suppress("UNCHECKED_CAST")
internal class LyVDFToolTest {

    val FILES_ROOT = "./src/test/resources/files"

    @Test
    fun searchAndParseBlock(){
        val vdfMap = LyVDFTool.searchAndParseFromFile("child1", "$FILES_ROOT/complex-block.vdf")

        assertThat(vdfMap, Matchers.notNullValue())
        assertThat(vdfMap.size, Matchers.equalTo(1))

        val rootBlock = vdfMap["child1"] as SortedMap<String, Any>

        assertThat(rootBlock["child1key1"] as String, Matchers.equalTo("child1value1"))
        assertThat(rootBlock["child1key2"] as String, Matchers.equalTo("child1value2"))

    }

    @Test
    fun searchAndParseBlockToJSON(){
        val vdfMap = LyVDFTool.searchAndParseFromFile("child1", "$FILES_ROOT/complex-block.vdf")

        assertThat(vdfMap, Matchers.notNullValue())
        assertThat(vdfMap.size, Matchers.equalTo(1))

        val rootBlock = vdfMap["child1"] as SortedMap<String, Any>

        assertThat(rootBlock["child1key1"] as String, Matchers.equalTo("child1value1"))
        assertThat(rootBlock["child1key2"] as String, Matchers.equalTo("child1value2"))

        val jsonResult = LyVDFTool.mapToJSON(vdfMap)
        assertThat(jsonResult, Matchers.notNullValue())

    }
}