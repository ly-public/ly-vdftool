### Introduction

This is a simple tool that can help you handle Valve's VDF files, parse, search, and convert to JSON.
This was created because I need just simple tool and I don't feel comfortable with tools available at the time.

### Supported features

Read VDF content from:
- Files
- List of strings
- Multi-line strings

You can read VDF files with the following:
- Lines with key-values
- Lines with key-blocks
- Lines with key and multi-line values.

Other features:
- Search and extract the first occurence of specific block from file
- A function to convert the generated output to JSON using Gson.

__NOTE: This parser do not recognizes types, all values are strings.__


###Usage
Most of this methods returns a sortedMap, with the exception of mapToJSON, it is self-explanatory. 

From file:

```
val sortedMap = LyVDFTool.parseFromFile("path/to/your/file.vdf")
```

From string (multi-line):

```
val sortedMap = LyVDFTool.parseFromString(""" {... vdf content ...} """)
```

From a list of strings:
```
val sortedMap = LyVDFTool.parseFromList(listOf( {... vdf elements ...} ))
```

From file content search:
```
val sortedMap = LyVDFTool.searchAndParseFromFile("keyWord", "path/to/source/file.vdf" )
```

Search feature will avoid a huge map with unnecessary information, using the keyword the search feature will look for specific block of information and returns first occurrence as a map.

### Bugs / Suggestions / Contact
Feel free to raise a ticket if you encounter a bug, also you can suggest things or just say thank you [@arlyoneel](https://twitter.com/arlyoneel) on Twitter.  

### License
LyVDFTool is released under [MIT license](LICENSE.txt).
